<?php
session_start();	
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Mes informations</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>
  	
	<div class="jumbotron">
  		<div class="container">
  			<h1>Mes informations</h1>
  		</div>
  	</div>

  	<div class="container">
  		<!-- Example row of columns -->
  		
  		<div class="row">
  			<div class="col-md-2"></div>
  			<div class="col-md-8">
  				<div class="panel panel-default">
  					<div class="panel-heading">
  						<h3 class="panel-title">Mes informations</h3>
  					</div>
  					<div class="panel-body">
  						<div class="row">
  							<div class="col-md-12">
  								<table class="table">
  									<?php	
  									if (isset($_SESSION['login']) and $_SESSION['role']=="admin"){
  										
  										?>
  										<tr>
  											<td class="text-right"> <b> Login : </b> </td>
  											<td><?php echo $_SESSION['login']; ?> </td>
  										</tr>	
  										<?php
  									}else{
  										?>	
  										<tr>
  											<td class="text-right"> <b> Nom : </b> </td>
  											<td id="nom"><?php echo $_SESSION['nom']; ?> </td>
  											<td><button class="btn btn-default" id="buttonomf" onclick="modif(this.id)">Modifier</button></td>
  										</tr>
  										<tr>
  											<td></td>
  											<td colspan=2>
  												<div class="input-group hidden" id="nomf">
  													<input type="text" class="form-control" id="nom_modif">
  													<span class="input-group-btn">
  														<button class="btn btn-default" type="button" onClick="valid_modif()">Valider</button>
  													</span>
  												</div>
  											</td>
  										</tr>
  										<tr>
  											<td class="text-right"> <b> Prénom : </b> </td>
  											<td><?php echo $_SESSION['prenom'];?> </td>
  											<td><button class="btn btn-default" id="buttoprenomf" onclick="modif(this.id)">Modifier</button></td>
  										</tr>
  										<tr>
  											<td></td>
  											<td colspan=2>
  												<div class="input-group hidden" id="prenomf">
  													<input type="text" class="form-control" id="prenom_modif">
  													<span class="input-group-btn">
  														<button class="btn btn-default" type="button" onClick="valid_modif()">Valider</button>
  													</span>
  												</div>
  											</td>
  										</tr>
  										<tr>
  											<td class="text-right"> <b> Email : </b> </td>
  											<td id="email"><?php echo $_SESSION['login']; ?></td>
  											<td><button class="btn btn-default" id="buttoemailf" onclick="modif(this.id)">Modifier</button></td>
  										</tr>
  										<tr>
  											<td></td>
  											<td colspan=2>
  												<div class="input-group hidden" id="emailf">
  													<input type="text" class="form-control" id="email_modif">
  													<span class="input-group-btn">
  														<button class="btn btn-default" type="button" onClick="valid_modif()">Valider</button>
  													</span>
  												</div>
  												<span class="text-danger hidden" id="email_alert">Merci de rentrer une adresse mail valide</span>
  												
  											</td>
  										</tr>
  										<tr>
  											<td class="text-right"> <b> Date de naissance : </b> </td>
  											<td><?php echo $_SESSION['date_naissance']; ?> </td>
  											<td><button class="btn btn-default" id="buttodatenf" onclick="modif(this.id)">Modifier</button></td>
  										</tr>
  										<tr>
  											<td></td>
  											<td colspan=2>
  												<div class="input-group hidden" id="datenf">
  													<input type="text" class="form-control" id="date_naiss_modif">
  													<span class="input-group-btn">
  														<button class="btn btn-default" type="button" onClick="valid_modif()">Valider</button>
  													</span>
  												</div>
  												<span class="text-danger hidden" id="datenaiss_alert"></span>
  											</td>
  										</tr>
  										<?php
  									};
  									?>
  								</table>
  								<p class="text-center">
  									<a class="btn btn-default" href="modif_mail.php">Modifier mon mot de passe</a>
  								</p>
  							</div>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-2"></div>
  			</div>
  		</div>

  		<hr>

  		<footer>
  			<p>&copy;</p>
  		</footer>
  	</div> <!-- /container -->	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
	    //Affichage de l'input pour modifier
	    function modif(clicked_id){
	    	$type = clicked_id.slice(5);
	    	if($("#" + $type).hasClass("hidden")){
	    		$("#" + $type).removeClass("hidden");
	    	}else{
	    		$("#" + $type).addClass("hidden");
	    	}
	    };
	    
	    function valid_modif(){
	    	var new_nom = $("#nom_modif").val();
	    	var new_prenom = $("#prenom_modif").val();
	    	var new_email = $("#email_modif").val();
	    	var new_daten = $("#date_naiss_modif").val();
	    	var email = $("#email").text();
	    	
	    	if(!new_email.length==0){
	    		ok_mail = verifMail(new_email);
	    	}else { ok_mail = true;}
	    	if(!new_daten.length==0){
	    		ok_date = verifDDNEtAge(new_daten);
	    	}else { ok_date = true;}
	    	
	    	if ( ok_mail && ok_date ){
	    		$.ajax({
	    			type : "POST",
	    			url : "fonction.php",
	    			data : {new_nom : new_nom, new_prenom : new_prenom, new_email : new_email, new_daten : new_daten, email : email},
	    			dataType: 'json',
	    			success: function (json) {
	    				if(json['etat']=="ok"){		
	    					location.href = location.href;
	    				}
	    			}
	    		});
	    	};
	    };
	    
	    function verifDDNEtAge(date)
	    {
	    	var decoupe = date.split('/');
	    	
	    	var vdate = new Date(decoupe[2], decoupe[1]-1, decoupe[0]);
	    	var now = new Date();
	    	var ageMin = new Date(now.getFullYear()-18,now.getMonth(),now.getDate());
	    	if (vdate.getFullYear() == decoupe[2] && vdate.getMonth()+1 == decoupe[1] && vdate.getDate() == decoupe[0] && ageMin > vdate)
	    	{
	    		$("#datenaiss_alert").addClass("hidden");
	    		return true;
	    	}
	    	else{
	    		if(ageMin < vdate){
	    			$("#datenaiss_alert").text("Vous devez être majeur.");
	    		}else{
	    			$("#datenaiss_alert").text("Merci de rentrer une date de naissance au format jj/mm/aaaa.");
	    		}
	    		$("#date_naiss_modif").focus();
	    		$("#datenaiss_alert").removeClass("hidden");
	    		return false;
	    	}
	    }
	    
	    function verifMail(email)
	    {
	    	var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
	    	if(!regex.test(email))
	    	{
	    		$("#email_modif").focus();
	    		$("#email_alert").removeClass("hidden");
	    		return false;
	    	}
	    	else
	    	{
	    		$("#email_alert").addClass("hidden");
	    		return true;
	    	}
	    }
	    
	    </script>
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	    <script src="dist/js/bootstrap.min.js"></script>
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>
	</html>
