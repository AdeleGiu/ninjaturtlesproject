<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Recherche d'artiste</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>

  	
  	<div class="jumbotron">
  		<div class="container">
  			<h1>Recherche d'artiste</h1>
  		</div>
  	</div>

  	<div class="container">
  		
  		<div class="row">
  			<div class="col-md-12">
  				<div class="panel panel-default">
  					<div class="panel-heading">
  						<h3 class="panel-title">Recherche d'artiste</h3>
  					</div>
  					<div class="panel-body">
  						<form class="form-inline">
  						 <div class="form-group">
  						     <label for="">Nom :</label>
     						 <input type="text" class="form-control" placeholder="Nom" id="name" value="<?php  if(isset($_GET['artist'])) echo $_GET['artist'] ?>">
							 
      						 <select name="list" size="1" class="selectpicker" id="type">
								<option value="Painting"> Peintre </option>
								<option value="Sculpture"> Sculpteur </option>
								<option value="Drawing"> Dessinateur </option>
								<option value="Cartoonist"> Dessinateur de BD </option>
								<option value="Author"> Ecrivain </option>
								<option value="Actor"> Acteur </option>
								<option value="Poetry"> Poete </option>
							</select>	
       							<button class="btn btn-primary" type="button" onclick="recherche()">Rechercher!</button>
      							
   						 </div><!-- /input-group -->
   						</form>
   						 <br/>
   						 <div>
   						 		<table class="table table-striped table-bordered">
   						 			<thead>
												<th>Nom</th>
												<th>Détails</th>
												<?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?> 
												<th></th>
												<?php }; ?>
   						 			</thead>
   						 			<tbody id="result">
   						 				
   						 			</tbody>
   						 		</table>
   						 </div>
  					</div>
  				</div>
  			</div>
  		</div>  	
  	</div>
		
  	<hr>
	<footer>
  		<p>&copy; </p>
  	</footer>
  		
  	 <!-- /container -->	
  	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	

    <script>
		function recherche(){
			
			<?php  if(isset($_GET['artist'])) echo "$('#type').val('".$_GET['genre']."');" ?>
			
		
			ajout_historique();
			var url = "https://dbpedia.org/sparql";
			var type=$("#type").val();
			var name = $("#name").val();
			
			var query = "\
				PREFIX dbpedia2: <http://dbpedia.org/property/>\
				PREFIX foaf: <http://xmlns.com/foaf/0.1/>\
				SELECT * WHERE {\
						  ?p a dbo:Artist .\
						  ?p dbpedia2:name ?name.\
						  ?p dbo:birthDate ?birthDate.\
						  ?p dbo:birthPlace ?birthPlace.\
						  ?p dbo:abstract ?abstract.\
						  ?p dbo:movement ?movement.\
						  ?p dbo:field ?field.\
						  FILTER regex(?name, \""+ name +"\")\
						  FILTER regex(?field, \""+ type +"\")\
						  FILTER langMatches(lang(?abstract),\'fr\')\
			}";
			
			var queryUrl = encodeURI( url+"?query="+query+"&format=json" );
			
			
			$.ajax({
				dataType: "jsonp",  
				url: queryUrl,
				success: function(data) {
					
					$( '#result' ).html("");
					var results = data.results.bindings;
					if(results.length === 0){
							var row = '<tr><td rowspan="2"> Aucun résultat trouvé </td><tr>';
							$( '#result' ).append(row);
					}else{
							var tmp="";
							$.each(results, function(idx,obj) {
							if(obj.birthDate.value != tmp){
								var row = '<tr><td>' + obj.name.value + '</td><td>' + obj.abstract.value + '</td><?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?><td><button class="btn btn-primary" type="button" onclick="ajout_like(\''+ obj.name.value +'\',\''+ type +'\')">Favori</button> </td><?php }; ?></tr>';
								console.log(row);
								$( '#result' ).append(row);
							};					
							tmp = obj.birthDate.value;
						});
					};
					
				}
			});		
		};
		
		
		 //Ajouter a l'historique
		function ajout_historique(){
		var artist = $('#name').val();
		var genre = $('#type').val();
		
		
		if(genre == ""){
			genre ="";
		}
		if(artist == ""){
			artist = "";
		}
		
		var email ="<?php echo $_SESSION['login']?>";
	    var type = "art";
		
	    $.ajax({
	    	type : "POST",
	    	url : "fonction.php",
	    	data : {artist : artist, genre  : genre, email : email, type : type},
	    	dataType : 'json',
	    	success: function(json){
	    	}
	    });     
	};
	
	//Ajouter aux elements likes
	function ajout_like(genre, artist){
		
		if(genre == ""){
			genre ="";
		}
		if(artist == ""){
			artist = "";
		}
		
		var email ="<?php echo $_SESSION['login']?>";
	    var like = "art";
		
	    $.ajax({
	    	type : "POST",
	    	url : "fonction.php",
	    	data : {artist : artist, genre  : genre, email : email, like : like},
	    	dataType : 'json',
	    	success: function(json){
	    	}
	    });    
	};
	   
	   
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
	<?php  if(isset($_GET['artist'])) echo "<script> recherche();</script>" ?>
		
</body>
</html>
