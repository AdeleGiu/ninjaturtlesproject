<?php
session_start();	
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Les poneys sauvages</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>

  	<!-- Main jumbotron for a primary marketing message or call to action -->
  	<div class="jumbotron">
  		<div class="container">
  			<h1>Les poneys sauvages</h1>
  			<p>Bienvenue sur le site des poneys sauvages, votre site de vente en ligne d'équipement de sport extrême.</p>
  		</div>
  	</div>

  	<div class="container">
  		<!-- Example row of columns -->
  		
  		<div class="row">
  			<div class="col-md-2"></div>
  			<div class="col-md-8">
  				<div class="panel panel-default">
  					<div class="panel-heading">
  						<h3 class="panel-title">Modifier mon mot de passe</h3>
  					</div>
  					<div class="panel-body">
  						<div class="row">
  							<div class="col-md-12 text-center">
  								<form role="form" id="modifmdp">
  									<div class="form-group">
  										<label for="old_mdp">Ancien mot de passe:</label>
  										<input type="password" class="form-control" id="old_mdp">
  										<span class="hidden text-danger" id="mdp_wrong_alert">Erreur sur votre mot de passe.</span>
  									</div>
  									<div class="form-group">
  										<label for="new_mdp">Nouveau mot de passe:</label>
  										<input type="password" class="form-control" id="new_mdp">
  									</div>
  									<div class="form-group">
  										<label for="new_mdp2">Répéter le nouveau mot de passe:</label>
  										<input type="password" class="form-control" id="new_mdp2">
  										<span class="hidden text-danger" id="mdp_diff_alert">Les deux mots de passe ne sont pas identiques.</span>
  									</div>
  									<input class="hidden" id="email" value="<?php echo $_SESSION['login'] ?>">
  									
  									<button type="submit" class="btn btn-default">Valider</button>
  								</form>
  							</div>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-2"></div>
  			</div>
  		</div>

  		<hr>

  		<footer>
  			<p>&copy; 2016 LPS</p>
  		</footer>
  	</div> <!-- /container -->	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    	
    	$("#modifmdp").on("submit", function(e){
    		e.preventDefault();
    		$("#mdp_wrong_alert").addClass("hidden");
            var $this = $(this); // L'objet jQuery du formulaire
            
            // Je récupère les valeurs
            var old_mdp = $('#old_mdp').val();
            var new_mdp = $('#new_mdp').val();
            var email = $("#email").val();
            
            if( new_mdp != $('#new_mdp2').val()){
            	$("#mdp_diff_alert").removeClass("hidden");
            }else{
            	$("#mdp_diff_alert").addClass("hidden");
            	$.ajax({
            		type : "POST",
            		url : "fonction.php",
            		data:{new_mdp : new_mdp, old_mdp : old_mdp, email : email},
            		dataType: 'json',
            		success: function (json) {
            			if(json['etat']=="ok"){
            				location.href = "compte.php";
            			}else{
            				$("#mdp_wrong_alert").removeClass("hidden");
            			}
            		}
            	});
            };
        });

});

</script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
