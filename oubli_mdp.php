<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Loreum ipsum</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>

  	<div class="jumbotron">
  		<div class="container">
  			<h1>Loreum ipsum</h1>
  			<p>Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. </p>
  		</div>
  	</div>
  	
  	<div class="container">
  		<!-- Example row of columns -->

  		<div class="row">
  			<div class="col-md-4"></div>
  			<div class="col-md-4 text-center">
  				<p> 
  					<span class="text-info"> Entrez votre e-mail </span>
  				</p>		      
  				<form class="form" method="post" action="" id="reinit_mdp">
  					<div class="form-group">
  						<input type="text" placeholder="Email" id="login" class="form-control" name="login">
  					</div>
  					<button type="submit" class="btn btn-success">Recuperer un nouveau mot de passe</button>
  				</form>

  			</div>
  			<div class="col-md-4"></div>
  		</div>
  		
  		<!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
        
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <p id="modal-text"></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
        
          </div>
        </div>

  		<hr>
  		
  		<footer>
  			<p>&copy;</p>
  		</footer>
  	</div> <!-- /container -->	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <?php
        if(isset($_POST['login']))
        {
        
            $base = mysql_connect ($bdd_adress, $bdd_login, $bdd_password) or die("Impossible de se connecter : " . mysql_error());
            mysql_select_db ($bdd_name, $base);
            mysql_query("SET character_set_results=utf8", $base);
            mb_language('uni'); 
            mb_internal_encoding('UTF-8');
            mysql_query("set names 'utf8'",$base);
            
            		
            $sql = "SELECT * FROM utilisateur WHERE u_email='".$_POST['login']."'";
            $req = mysql_query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysql_error());
            $user = mysql_fetch_array($req);
            
            if(!empty($user))
            {
             
                $mail = $_POST['login']; // Déclaration de l'adresse de destination.
                
                if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui présentent des bogues.
                {
                
                    $passage_ligne = "\r\n";
                
                }else{
                
                    $passage_ligne = "\n";
                
                }
                
                //=====Déclaration des messages au format texte.
                
                $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $mot_de_passe = str_shuffle($char);
                $mot_de_passe = substr($mot_de_passe,0,5);
                $mot_de_passe_crypte = sha1($CSecrete1.$mot_de_passe.$CSecrete2);
                
                $sql = "UPDATE utilisateur SET u_mdp ='".$mot_de_passe_crypte."' WHERE u_email = '".$_POST['login']."';";
                $req = mysql_query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysql_error());
                
                $message_txt = "Bonjour, votre mot de passe a été réinitialisé et est maintenant:\n".$mot_de_passe;
                
                //==========
                
                 
                
                //=====Création de la boundary.
                
                $boundary = "-----=".md5(rand());
                
                $boundary_alt = "-----=".md5(rand());
                
                //==========
                
                 
                
                //=====Définition du sujet.
                
                $sujet = "Votre nouveau mot de passe";
                
                //=========
                
                 
                
                //=====Création du header de l'e-mail.
                
                $header = "From: \"Les Poneys Sauvages\"<noreply@ebriand.fr>".$passage_ligne;
                
                $header.= "Reply-to: \"\" <contact@ebriand.fr>".$passage_ligne;
                
                $header.= "MIME-Version: 1.0".$passage_ligne;
                
                $header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
                
                //==========
                
                 
                
                //=====Création du message.
                
                $message = $passage_ligne."--".$boundary.$passage_ligne;
                
                $message.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary_alt\"".$passage_ligne;
                
                $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
                
                //=====Ajout du message au format texte.
                
                $message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
                
                $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
                
                $message.= $passage_ligne.$message_txt.$passage_ligne;
                //=====On ferme la boundary alternative.
                
                $message.= $passage_ligne."--".$boundary_alt."--".$passage_ligne;
                
                //==========
                $message.= $passage_ligne."--".$boundary.$passage_ligne;
                
                
                //=====Envoi de l'e-mail.
                
                mail($mail,$sujet,$message,$header);
                
             
                echo"
      		    <script type='text/JavaScript'> 
      	            alert('Votre nouveau mot de passe a bien été envoyé. Si vous ne trouvez pas le mail, regardez dans votre boite spam.');
      	        </script>";
                //==========
               
            }else{
                echo"
      		    <script type='text/JavaScript'> 
      	            alert('Il n'existe pas d'utilisateur avec cet e-mail.');
      	        </script>";
            }
        };
    ?>
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>


