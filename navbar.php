 <?php
 include_once("info_bdd.php");
 ?>
 <link href="navbar.css" rel="stylesheet">
 <nav class="navbar navbar-inverse navbar-fixed-top">
 	<div class="container">
 		<div class="navbar-header">
 			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
 				<span class="sr-only">Toggle navigation</span>
 				<span class="icon-bar"></span>
 				<span class="icon-bar"></span>
 				<span class="icon-bar"></span>
 			</button>
 			<a class="navbar-brand" href="index.php">Accueil</a>
 			<a class="navbar-brand" href="music.php">Musique</a>
 			<a class="navbar-brand" href="film.php">Film</a>
 			<a class="navbar-brand" href="art.php">Art</a>
 		</div>
 		<div id="navbar" class="navbar-collapse collapse">
 			<?php
 			$base = mysql_connect ($bdd_adress, $bdd_login, $bdd_password) or die("Impossible de se connecter : " . mysql_error());
 			mysql_select_db ($bdd_name, $base);
 			mysql_query("SET character_set_results=utf8", $base);
 			mb_language('uni'); 
 			mb_internal_encoding('UTF-8');
 			mysql_query("set names 'utf8'",$base);
 			
 			if(!isset($_SESSION['login'])){
 				?>
 				<form class="navbar-form navbar-left" method="post">
 					<div class="form-group">
 						<input type="text" placeholder="Email" id="login" class="form-control" name="login">
 					</div>
 					<div class="form-group">
 						<input type="password" placeholder="Mot de passe" id="mdp" class="form-control" name="mdp">
 					</div>
 					<button type="button" class="btn btn-success" onClick="connexion()">Se connecter</button>
 					
 				</form>
 				<a href="inscription.php"><button type="button" class="btn btn-primary navbar-btn">S'inscrire</button></a>
 				<?php
 			}else if (isset($_SESSION['login']) and $_SESSION['role']=="user"){
 				?>
 				<div class="navbar-right">
 					<p class="navbar-text"><span class="label label-primary" style="font-size:14px;">Bonjour <?php echo $_SESSION['prenom'] ?> <?php echo $_SESSION['nom'] ?> !</span></p>
 					<ul class="nav navbar-nav">
 						<li class="dropdown">
 							<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Mon compte<span class="caret"></span></a>
 							<ul class="dropdown-menu dropdown-menu-right">
 							 	<li><a href="compte.php">Mes informations</a></li>
 							 	<li><a href="history.php">Mon historique</a></li>
 								<li><a href="#" onclick="deconnexion()">Me déconnecter</a></li>
 							</ul>
 						</li>
 					</ul>
 					
 				</div>
 				
 				<?php	
 			}else if (isset($_SESSION['login']) and $_SESSION['role']=="admin"){
 				?>
 				
 				<div class="navbar-right">
 					<ul class="nav navbar-nav">
 						<li class="dropdown">
 							<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Mon compte<span class="caret"></span></a>
 							<ul class="dropdown-menu dropdown-menu-right">
 								<li><a href="compte.php">Mes informations</a></li>
 							 	<li><a href="admin.php">Gestion administrateur</a></li>
 								<li><a href="#" onclick="deconnexion()">Me déconnecter</a></li>
 							</ul>
 						</li>
 						
 					</div>		
 					<?php
 				};
 				mysql_close($base);
 				?>
 			</div><!--/.navbar-collapse -->
 		</div>
 	</nav>
 	<script type="text/javascript">
 	function deconnexion() 
 	{
 		$.ajax({
 			type : "POST",
 			url : "fonction.php",
 			data : {deconnexion:"true"},
 			success: function () {
 				window.location.href = "index.php";
 			}
 		})
 	};	
 	
 	function connexion() 
 	{
 		var login = $('#login').val();
 		var mdp = $('#mdp').val();
 		$.ajax({
 			type : "POST",
 			url : "fonction.php",
 			data : {login : login, mdp : mdp},
 			dataType: 'json',
 			success: function (json) {
 				if(json.etat=="ok"){
 					window.location.href = window.location.href;   
 				}else{
 					window.location.href = "connect_error.php";
 				}
 			}
 		})
 	};	
 	</script>