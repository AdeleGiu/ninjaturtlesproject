<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Recherche de film</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>

<div class="jumbotron">
  		<div class="container">
  			<h1>Recherche de film</h1>
  			<p> </p>
  		</div>
  	</div>

  	<div class="container">
  		
  		<div class="row">
  			<div class="col-md-12">
  				<div class="panel panel-default">
  					<div class="panel-heading">
  						<h3 class="panel-title">Recherche de film</h3>
  					</div>
  					<div class="panel-body">
  						<form class="form-inline">
  						 <div class="form-group">
  						     <label for="film">Film :</label>
     						 <input type="text" class="form-control" placeholder="Nom de film..." id="film">
     						 <label for="annee">Annee :</label>
     						 <input type="text" class="form-control" placeholder="Annee..." id="annee">
							 <label for="type">Type :</label>
							 <select name="list" size="1" class="selectpicker" id="mylist">
								<option>Film</option>
								<option>Serie</option>
								<option>Episode</option>
							</select> 
       								 <button class="btn btn-primary" type="button" onclick="recherche_film_main()">Rechercher!</button> 
   						 </div><!-- /input-group -->
   						</form>
   						 <br/>
   						 <div>
   						 		<table class="table table-striped table-bordered">
   						 			<thead id ="entete">

   						 			</thead>
   						 			<tbody id="result">
   						 				
   						 			</tbody>
   						 		</table>
   						 </div>
  					</div>
  				</div>
  			</div>
  		</div>  	
  	</div>
		
  	<hr>
	<footer>
  		<p>&copy; </p>
  	</footer>
  		
  		
  	 <!-- /container -->	
  	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>
	function recherche_film_main(){
		ajout_historique();
    		if( $("#film").val() != "" && $("#annee").val() == "" ){
	    		recherche_film();
				
	    	}
	    	else if( $("#annee").val() != ""  && $("#film").val() != "" ){
			recherche_annee();}
    	};
    

 function recherche_film(){
 			var  tempRequete = $("#mylist").val() ;  // series // episode // movie 
	    	var tempTab = tempRequete ;
	    	if(tempRequete == "Film"){
	    		tempRequete = "movie";
	    	}else if (tempRequete == "Serie"){
	    		tempRequete = "series";
	    	}else if(tempRequete == "Episode"){
	    		alert("l\' API  ne retourne rien !");
	    		return ;
	    	}
	    	var film = $("#film").val();
	    		console.log(tempRequete);
	    		$.ajax({
	    			type : "GET",
	    			
	    			url : "https://omdbapi.com/?s="+film + "&type="+ tempRequete,
	    			
	    			dataType: 'json',
	    			success : function (data) {
	    				console.log(data);
						
	    				$("#result").html("");
	    				$("#entete").html("");
	    				$("#entete").append("<tr><th>"+ tempTab +"</th><th>Annee </th><th>Image</th><?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?> <th></th>	<?php }; ?></tr>");
	    				$.each(data.Search , function(idx,obj) {
	    					var row = '<tr><td>' + obj.Title;
	    					var nimp;
							$.ajax({
								type : "GET",
	    						url : "https://omdbapi.com/?t="+obj.Title + "&type="+ tempRequete,
	    						dataType: 'json',
	    						success : function(databis){
	    							//console.log(databis);
	    							var row = '<tr><td>' + obj.Title + '<br/>' + 'Durée : ' + databis.Runtime;
	    							row += '<br/>' + 'Genre : ' + databis.Genre;
	    							row += '<br/>' + 'Acteurs : ' + databis.Actors;
	    							row += '<br/>' + 'Réalisateur : ' + databis.Director;
	    							row += '<br/>' + 'Langue : ' + databis.Language;
	    							if(databis.Website!="N/A"){
	    								row += '<br/>' + '<a href="'+ databis.Website + '" onclick="window.open(this.href); return false;">lien vers le site du film</a>';
	    							}
	    							row += '</td>'
	    							row += '<td>' + obj.Year + '</td>';
	    							row += '<td><img width=100 src="'+ obj.Poster +'" class="img-responsive" alt="image"></td>';
									<?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?>
	    							row+= '<td><button class="btn btn-primary" type="button" onclick="ajout_like(\''+ databis.Genre+'\',\''+ obj.Title +'\',\''+ obj.Year +'\')">Favori</button></td>';
									<?php }; ?>
									row+='</tr>';
	    							$("#result").append(row);
	    						}
							});
						});
					}
	    		});
	   
	    };
function recherche_annee(){
			var annee = $("#annee").val();
	    	var film = $("#film").val();
	    	
	    		$.ajax({
	    			type : "GET",
					url : "https://omdbapi.com/?s="+film+"&y="+annee,
	    			dataType: 'json',
	    			success : function (data) {
	    				console.log(data);
	    				$("#result").html("");
	    				$("#entete").html("");
	    				$("#entete").append("<tr><th>Film</th><th>Annee </th><th>Image</th><?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?> <th></th>	<?php }; ?></tr>");
	    				$.each(data.Search , function(idx,obj) {
	    				var row = '<tr><td>' + obj.Title + '</td>';
						row += '<td>' + obj.Year + '</td>';
						row += '<td><img width=100 src="'+ obj.Poster +'" class="img-responsive" alt="image"></td>';
						<?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?>
	    			    row+= '<td><button class="btn btn-primary" type="button" onclick="ajout_like(\''+ "" +'\',\''+ obj.Title +'\',\''+ obj.Year +'\')">Favori</button></td>';
						<?php }; ?>
						row+='</tr>';
	    				$("#result").append(row);

						});
				}
	    		});
	    };
	
	//Ajouter a l'historique
	function ajout_historique(){
			
		var artist = $('#film').val();
		var album = $('#annee').val();
		var genre = $('#mylist').val();
		if(genre == ""){
			genre ="";
		}
		if(album == ""){
			album ="";
		}
		if(artist == ""){
			artist = "";
		}
		var email ="<?php echo $_SESSION['login']?>";
	    var type = "film";
	   //console.log(artist +email + type+ label+album +genre );
	    $.ajax({
	    	type : "POST",
	    	url : "fonction.php",
	    	data : {artist : artist, album : album, genre  : genre , email : email, type : type},
	    	dataType : 'json',
	    	success: function(json){
	    		    	//console.log(artist + email + json);
	    	},  error: function(json){
	    			   // console.log(json);
						//console.log("erreur");
	    	}
	    });     
	};
    	//Ajouter aux elements likes
	function ajout_like(genre, artist, album ){
		if(album == ""){
			album ="";
		}
		if(genre == ""){
			genre ="";
		}
		if(artist == ""){
			artist = "";
		}
		
		var email ="<?php echo $_SESSION['login']?>";
	    var like = "film";
		
	    $.ajax({
	    	type : "POST",
	    	url : "fonction.php",
	    	data : {album : album , artist : artist, genre  : genre, email : email, like : like},
	    	dataType : 'json',
	    	success: function(json){
	    	}
	    });    
	};
	 
		
		
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

</body> 
</html>
