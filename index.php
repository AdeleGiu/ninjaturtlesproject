<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Accueil</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>

  	
  	<div class="jumbotron">
  		<div class="container">
  			<h1> Ninj'art </h1>
  			<p>Bienvenue sur notre site de recherche de contenu artistique </p>
  		</div>
  	</div>
	
  	<div class="container" >	
  		<div class="row">
	<?php
		if (isset($_SESSION['login']) and $_SESSION['role']=="user"){		
			
			$ligne ="<h2>Mes favoris</h2></br>";
			echo $ligne;
			
				
			$base = mysql_connect ($bdd_adress, $bdd_login, $bdd_password) or die("Impossible de se connecter : " . mysql_error());
			mysql_select_db ($bdd_name, $base);
			mysql_query("SET character_set_results=utf8", $base);
			mb_language('uni'); 
			mb_internal_encoding('UTF-8');
			mysql_query("set names 'utf8'",$base);
			setlocale (LC_TIME, 'fr_FR.utf8','fra');   		
  		    
            $sql = 'SELECT * FROM favori WHERE (favori.u_mail="'.$_SESSION['login'].'") ORDER BY f_type';
  		    $favs = mysql_query($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
		   $tmp = '';
		   
      		while($donnees = mysql_fetch_array($favs))
      		{  
					
					$ligne = "";
					$enteteSpe ="";
					if($donnees['f_type']!= $tmp){
						
							if($tmp!=''){
								$ligne ="</tbody></table>";
								echo $ligne;	
							}

							
							if($donnees['f_type']== "art"){
								$type="Artistes";
								$enteteSpe = "<th>Type d'artiste</th><th>Nom d'artiste</th>";
							}						
							if($donnees['f_type']=="music"){
								$type="Musiques";
								$enteteSpe = "<th>Nom d'artiste ou groupe </th><th>Nom du label</th><th>Genre de musique </th><th>Nom du morceau</th><th>Nom de l'album</th>";

							}
							if($donnees['f_type']=="film"){
								$type="Films & Séries";
								$enteteSpe = "<th>Annee de sortie</th><th>Nom du film</th><th>Genre du film</th>";
							}
							
							$ligne ="<h3>".$type."</h3></br>";					
							$ligne .="<table class='table table-striped'><thead><tr>".$enteteSpe."</tr></thead><tbody>";
							echo $ligne;		
				}
 		
						$ligne =  "<tr>";
						if($donnees['f_type']== "art"){
							if($donnees['f_artist']!= null){
								$ligne .= "<td>".$donnees['f_artist']."</td>";
							}else{
								$ligne .= "<td></td>";
							}
							if($donnees['f_genre']!= null){
								$ligne .="<td>".$donnees['f_genre']."</td>";
							} else{
								$ligne .= "<td></td>";
							} 
						}
						if($donnees['f_type']== "music"){
							
							if($donnees['f_artist']!= null){
								$ligne .= "<td>".$donnees['f_artist']."</td>";
							}else{
								$ligne .= "<td></td>";
							}
							
							if($donnees['f_label']!= null){
								$ligne .="<td>".$donnees['f_label']."</td>";
							}else{
								$ligne .= "<td></td>";
							}
							if($donnees['f_genre']!= null){
								$ligne .="<td>".$donnees['f_genre']."</td>";
							} else{
								$ligne .= "<td></td>";
							} 
							if($donnees['f_track']!= null){
								$ligne .="<td>".$donnees['f_track']."</td>";
							}else{
								$ligne .= "<td></td>";
							}  
							if($donnees['f_album']!= null){
        	    			$ligne .= "<td>".$donnees['f_album']."</td>";
							}else{
								$ligne .= "<td></td>";
							}    
							
						}
						if($donnees['f_type']== "film"){
							if($donnees['f_album']!= null){
        	    				$ligne .= "<td>".$donnees['f_album']."</td>";
							}else{
								$ligne .= "<td></td>";
							}
							if($donnees['f_artist']!= null){
								$ligne .= "<td>".$donnees['f_artist']."</td>";
							}else{
								$ligne .= "<td></td>";
							}
							if($donnees['f_genre']!= null){
								$ligne .="<td>".$donnees['f_genre']."</td>";
							} else{
								$ligne .= "<td></td>";
							} 
						}
						
						 $ligne .= "</tr>" ;   
						echo $ligne;	
						
						$tmp = $donnees['f_type'];  	 			       		                 
  	        }
			$ligne ="</tbody></table>";
			echo $ligne;	
		}?>
  		</div>  	
  	 </div>
  	  
  	<hr>
	<footer>
  		<p>&copy; </p>
  	</footer>
  		
  	 <!-- /container -->	
  	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <script>
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>
