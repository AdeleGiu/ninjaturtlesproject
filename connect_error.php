<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Loreum ipsum</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>

  	<div class="jumbotron">
  		<div class="container">
  			<h1>Loreum ipsum</h1>
  			<p>Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. </p>
  		</div>
  	</div>
  	

  	<div class="container">
  		<!-- Example row of columns -->

  		<div class="row">
  			<div class="col-md-4"></div>
  			<div class="col-md-4 text-center">
  				<p> 
  					<span class="text-danger"> Erreur sur le couple login/mdp </span>
  				</p>		      
  				<form class="form" method="post">
  					<div class="form-group">
  						<input type="text" placeholder="Email" id="login2" class="form-control" name="login">
  					</div>
  					<div class="form-group">
  						<input type="password" placeholder="Mot de passe" id="mdp2" class="form-control" name="mdp">
  					</div>
  					<button type="button" class="btn btn-success" onClick="connexion2()">Se connecter</button>
  				</form>

  				<p> <a href="./oubli_mdp.php">J'ai oublié mon mot de passe.</a> </p>
  			</div>
  			<div class="col-md-4"></div>
  		</div>

  		<hr>

  		<footer>
  			<p>&copy;</p>
  		</footer>
  	</div> <!-- /container -->	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <script type="text/javascript">
    function connexion2() 
    {
    	var login = $('#login2').val();
    	var mdp = $('#mdp2').val();
    	$.ajax({
    		type : "POST",
    		url : "fonction.php",
    		data : {login : login, mdp : mdp},
    		dataType: 'json',
    		success: function (json) {
    			if(json.etat=="ok"){
    				window.location.href = "index.php"   
    			}else{
    				window.location.href = "connect_error.php";
    			}
    		}
    	})
    };	
    </script>  

</body>
</html>
