<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Recherche de musique</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
	<!-- Latest compiled and minified JavaScript -->
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>

  	
  	<div class="jumbotron">
  		<div class="container">
  			<h1>Recherche de musique</h1>
  			<p> </p>
  		</div>
  	</div>

  	<div class="container" >	
  		<div class="row">
  			<div class="col-lg-12">
  				<form class="form-inline">
  					<div class="form-group">
  						 <label for="artiste">Artiste :</label>
     					 <input type="text" class="form-control" placeholder="Nom d'artiste..." id="artiste" value ="<?php  if(isset($_GET['artist'])) echo $_GET['artist'] ?>">
     					 <label for="album">Album :</label>
     					 <input type="text" class="form-control" placeholder="Nom d'album..." id="album" value ="<?php  if(isset($_GET['album'])) echo $_GET['album'] ?>">
     					 <label for="track">Morceau :</label>
     					 <input type="text" class="form-control" placeholder="Nom du morceau..." id="track" value ="<?php  if(isset($_GET['track'])) echo $_GET['track'] ?>">
     				 </div>	
     				 <br/>
     				 <br/>
     				 <div class="form-group">
     					 <label for="label">Label :</label>
     					 <input type="text" class="form-control" placeholder="Nom du Label..." id="label" value ="<?php  if(isset($_GET['label'])) echo $_GET['label'] ?>" >
      					 <label for="label">Genre :</label>
      					 <select class="selectpicker" data-live-search="true" id="genre" value ="<?php  if(isset($_GET['genre'])) echo $_GET['genre'] ?>"></select>
       					 <button class="btn btn-primary" type="button" onclick="recherche_music_main();">Rechercher!</button>		
   					 </div><!-- /input-group -->
   				</form>
   				<br/>
   				<div>
   					<table class="table table-striped table-bordered">
   						 <thead id ="entete">
   						 </thead>
   						 <tbody id="result">			
   						 </tbody>
   					</table>
   				</div>
  			</div>
  		</div>  	
  	</div>
		
  	<hr>
	<footer>
  		<p>&copy; </p>
  	</footer>
  		
  	 <!-- /container -->	
  	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
    <script>
    	$(document).ready(function(){
    		console.log("https://api.deezer.com/genre/?limit=100&output=jsonp");
    		$.ajax({
	    			type : "GET",
	    			url : "https://api.deezer.com/genre?output=jsonp&limit=1",
	    			dataType: 'jsonp',
	    			success : function (data) {
	    				console.log(data);
	    				$("#genre").html("");
	    				$.each(data.data , function(idx,obj) {
	    				var row = '<option data-tokens="' + obj.name + '">' + obj.name +'</option>';
	    				$("#genre").append(row);
	    				});
	    			}
	    		});
    	});
    	
    	function recherche_music_main(){
  			<?php  if(isset($_GET['genre'])) echo "$('#type').val('".$_GET['genre']."');" ?>
    		ajout_historique();
    		if( $("#artiste").val() != "" && $("#album").val() == "" &&  $("#track").val() =="" &&  $("#label").val() == "" && $("#genre").val() == "Tous" ){
	    		recherche_artiste();
	    	}else if( $("#album").val() != ""  && $("#artiste").val() == "" &&  $("#track").val() =="" &&  $("#label").val() == "" && $("#genre").val() == "Tous"){
	    		recherche_album();
	    	}else if( $("#track").val() != ""  && $("#artiste").val() == "" &&  $("#album").val() =="" &&  $("#label").val() == "" && $("#genre").val() == "Tous"){
	    		recherche_track();
	    	}else{
	    		recherche_music();
	    	}
    	};
    	
    	function recherche_artiste(){
	    	var artiste = $("#artiste").val();
	    		$.ajax({
	    			type : "GET",
	    			url : "https://api.deezer.com/search/artist/?q="+artiste+"&output=jsonp",
	    			dataType: 'jsonp',
	    			success : function (data) {		
	    				console.log(data);
	    				$("#result").html("");
	    				$("#entete").html("");
	    				$("#entete").append("<tr><th>Artiste</th><th>Lien deezer</th><th>Image </th><th>Nombre d'albums </th><th>Nombre de fans deezer</th><?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?> <th></th>	<?php }; ?></tr>");
	    				$.each(data.data , function(idx,obj) {
	    				var row = "<tr><td>" + obj.name + "</td><td><a href='"+ obj.link + "' onclick='window.open(this.href); return false;'>lien vers la page deezer</a></td><td><img src='"+ obj.picture_small +"' class='img-responsive' alt='image'></td><td>" + obj.nb_album +"</td><td>" + obj.nb_fan +"<?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?><td><button class='btn btn-primary' type='button' onclick='ajout_like(\""+ "" +"\",\""+ obj.name +"\",\""+ "" +"\",\""+""+"\",\""+""+"\")'>Favori</button></td>	<?php }; ?></tr>";
	    				$("#result").append(row);
	    				});
	    			}
	    		});
	    };
	    
	   	function recherche_track(){
	    	var track = $("#track").val();
	    		$.ajax({
	    			type : "GET",
	    			url : "https://api.deezer.com/search/track/?q="+track+"&output=jsonp",
	    			dataType: 'jsonp',
	    			success : function (data) {
	    				console.log(data);
	    				$("#result").html("");
	    				$("#entete").html("");
	    				$("#entete").append("<tr><th>Artiste</th><th>Lien deezer</th><th>Image de l'artiste</th><th>Nom de l'album</th><th>Cover de l'album</th><th>Lien vers l'album</th><th>Nom du morceau</th><th>Classement deezer</th><?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?><th></th><?php }; ?></tr>");
	    				$.each(data.data , function(idx,obj) {
							var row = '<tr>';
	    					if(obj.artist != undefined){
	    						if(obj.artist.name != undefined ){
	    							row +=	'<td>' + obj.artist.name + '</td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    						if(obj.artist.link){
	    							row += '<td><a href="'+ obj.artist.link + '" onclick="window.open(this.href); return false;">lien vers la page deezer</a></td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    						if(obj.artist.picture_small !=undefined){
	    							row += '<td><img src="'+ obj.artist.picture_small + '" class="img-responsive" alt="image"></td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    					}
	    					if(obj.album != undefined ){
	    						row += '<td>' + obj.album.title + '</td>';// nom de l'album 
	    						row += '<td><img src="'+ obj.album.cover_small + '" class="img-responsive" alt="image"></td>';
	    						//row += '<td>' + obj.album.nb_tracks + '</td>';//nombre de morceaux dans l'album
	    					}
	    					row += '<td><a href="'+ obj.link + '" onclick="window.open(this.href); return false;">lien vers la page deezer</a></td>';//lien vers l'album		
	    					row += '<td>' + obj.title  + '</td>';
	    					row += '<td>' + obj.rank + '</td>';
							<?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?>
	    					row+="<td><button class='btn btn-primary' type='button' onclick='ajout_like(\""+ "" +"\",\""+ obj.artist.name +"\",\""+ "" +"\",\""+obj.title+"\",\""+obj.album.title+"\")'>Favori</button></td>" ;
							<?php }; ?>
	    					row += '</tr>';
	    					$("#result").append(row);
	    				});
	    			}
	    		});
	    };
	   	
	    function recherche_album(){
	    	var album = $("#album").val();
	    		
	    		$.ajax({
	    			type : "GET",
	    			url : "https://api.deezer.com/search/album/?q="+album+"&output=jsonp",
	    			dataType: 'jsonp',
	    			success : function (data) {
	    				console.log(data);
	    				$("#result").html("");
	    				$("#entete").html("");
	    				$("#entete").append("<tr><th>Artiste</th><th>Lien deezer</th><th>Image de l'artiste</th><th>Nom de l'album</th><th>Cover de l'album</th><th>Nombre de morceaux dans l'album</th><th>Lien vers l'album</th><?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?><th></th><?php }; ?></tr>");
	    				$.each(data.data , function(idx,obj) {
							var row = '<tr>';
	    					if(obj.artist != undefined){
	    						if(obj.artist.name != undefined ){
	    							row +=	'<td>' + obj.artist.name + '</td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    						if(obj.artist.link){
	    							row += '<td><a href="'+ obj.artist.link + '" onclick="window.open(this.href); return false;">lien vers la page deezer</a></td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    						if(obj.artist.picture_small !=undefined){
	    							row += '<td><img src="'+ obj.artist.picture_small + '" class="img-responsive" alt="image"></td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    					}
	    					row += '<td>' + obj.title + '</td>';// nom de l'album 
	    					row += '<td><img src="'+ obj.cover_small + '" class="img-responsive" alt="image"></td>';
	    					row += '<td>' + obj.nb_tracks + '</td>';//nombre de morceaux dans l'album
	    					row += '<td><a href="'+ obj.link + '" onclick="window.open(this.href); return false;">lien vers la page deezer</a></td>';//lien vers l'album	
								<?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?>
	    					row+='<td><button class="btn btn-primary" type="button" onclick="ajout_like(\''+ "" +'\',\''+ obj.artist.name +'\',\''+ "" +'\',\''+""+'\',\''+obj.title+'\')">Favori</button></td>' ;
							<?php }; ?>
	    					row += '</tr>';
	    					$("#result").append(row);
	    				});
	    			}
	    		});
	    };
	    
	     function recherche_music(){
	    	var artiste = $("#artiste").val();
	    	var album = $("#album").val();
	    	var track = $("#track").val();
	    	var label = $("#label").val();
	    	var genre = $("#genre").val();
	    	var requete = "https://api.deezer.com/search?q=";
	    	if(artiste != "" ){
	    		requete += 'artist:"' + artiste + '" ';
	    	}
	    	if(album != "" ){
	    		requete += 'album:"' + album + '" ' ;
	    	}
	    	if(track != "" ){
	    		requete += 'track:"' + track  + '" ';
	    	} 
	    	if(label != "" ){
	    		requete += 'label:"' + label  + '" ';
	    	}
	    	if(genre != "" ){
	    		requete += 'genre:"' + genre  + '" ';
	    	}
	    	console.log(requete);
	    		$.ajax({
	    			type : "GET",
	    			url : requete +"&output=jsonp",
	    		//url : "https://api.deezer.com/track/3135556&output=jsonp",
	    			//data : {new_nom : new_nom, new_prenom : new_prenom, new_email : new_email, new_daten : new_daten, email : email},
	    			dataType: 'jsonp',
	    			success : function (data) {
	    				console.log(requete);
	    				$("#result").html("");
	    				$("#entete").html("");
	    				$("#entete").append("<tr><th>Artiste</th><th>Lien deezer</th><th>Image de l'artiste</th><th>Nom de l'album</th><th>Cover de l'album</th><th>Lien vers l'album</th><th>Nom du morceau</th><th>Classement deezer</th><?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?><th></th><?php }; ?></tr>");
	    				$.each(data.data , function(idx,obj) {
							var row = '<tr>';
	    					if(obj.artist != undefined){
	    						if(obj.artist.name != undefined ){
	    							row +=	'<td>' + obj.artist.name + '</td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    						if(obj.artist.link){
	    							row += '<td><a href="'+ obj.artist.link + '" onclick="window.open(this.href); return false;">lien vers la page deezer</a></td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    						if(obj.artist.picture_small !=undefined){
	    							row += '<td><img src="'+ obj.artist.picture_small + '" class="img-responsive" alt="image"></td>';
	    						}else{
	    							row +=	'<td></td>';
	    						}
	    					}
	    					if(obj.album != undefined ){
	    						row += '<td>' + obj.album.title + '</td>';// nom de l'album 
	    						row += '<td><img src="'+ obj.album.cover_small + '" class="img-responsive" alt="image"></td>';
	    						//row += '<td>' + obj.album.nb_tracks + '</td>';//nombre de morceaux dans l'album
	    					}
	    					row += '<td><a href="'+ obj.link + '" onclick="window.open(this.href); return false;">lien vers la page deezer</a></td>';//lien vers l'album		
	    					row += '<td>' + obj.title  + '</td>';
	    					row += '<td>' + obj.rank + '</td>';
							<?php if (isset($_SESSION['login']) and $_SESSION['role']=="user"){	?>
							row+='<td><button class="btn btn-primary" type="button" onclick="ajout_like(\''+ genre +'\',\''+ obj.artist.name +'\',\''+ label +'\',\''+obj.title+'\',\''+obj.album.title+'\')">Favori</button></td>' ;
							<?php }; ?>
	    					row += '</tr>';
	    					$("#result").append(row);
	    				});
	    			}
	    		});
	    };
    //Ajouter a l'historique
	function ajout_historique(){
			
		var artist = $('#artiste').val();
		var album = $('#album').val();
		var track = $('#track').val();
		var label = $('#label').val();
		var genre = $('#genre').val();
		if(genre == "Tous"){
			genre ="";
		}
		if(album == ""){
			album ="";
		}
		if(artist == ""){
			artist = "";
		}
		if(track == ""){
			track ="";
		}
		if(label == ""){
			label ="";
		}
		var email ="<?php echo $_SESSION['login']?>";
	    var type = "music";
	   //console.log(artist +email + type+ label+album +genre );
	    $.ajax({
	    	type : "POST",
	    	url : "fonction.php",
	    	data : {artist : artist, album : album, track : track,label : label ,genre  : genre , email : email, type : type},
	    	dataType : 'json',
	    	success: function(json){
	    		    	//console.log(artist + email + json);
	    	},  error: function(json){
	    			   // console.log(json);
						//console.log("erreur");
	    	}
	    });     
	};
	//Ajouter aux elements likes
	function ajout_like(genre, artist,label,track, album){
		if(album == ""){
			album ="";
		}
		if(track == ""){
			track ="";
		}
		if(label == ""){
			label ="";
		}
		if(genre == ""){
			genre ="";
		}
		if(artist == ""){
			artist = "";
		}
		
		var email ="<?php echo $_SESSION['login']?>";
	    var like = "music";
		
	    $.ajax({
	    	type : "POST",
	    	url : "fonction.php",
	    	data : {album: album, track: track , label : label , artist : artist, genre  : genre, email : email, like : like},
	    	dataType : 'json',
	    	success: function(json){
	    	}
	    });    
	};
    </script>
     <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
<?php  if( isset($_GET['artist']) || isset($_GET['album'])|| isset($_GET['track']) || isset($_GET['genre']) || isset($_GET['label']) ) echo "<script>recherche_music_main();</script>" ?>
</body>
</html>
