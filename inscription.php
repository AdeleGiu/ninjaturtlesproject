<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Loreum ipsum</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>
  	
  	<!-- Main jumbotron for a primary marketing message or call to action -->
  	<div class="jumbotron">
  		<div class="container">
  			<h1>Loreum ipsum</h1>
  			<p>Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. </p>
  		</div>
  	</div>

  	<div class="container">
  		<!-- Example row of columns -->
  		<div class="row">
  			<div class="col-md-3">
  			</div>
  			<div class="col-md-6">
  				<form method="post" id="formInsc" action="inscription.php" onSubmit="return verifForm(this)">            
  					<fieldset class="form-group">
  						<label for="nom">Nom :</label>
  						<input type="text" class="form-control" id="nom" placeholder="Nom" onblur="verifNom(this)" name="nom">
  						<div id="nom_alert" class="text-danger hidden">Veuillez entrer un nom</div>
  					</fieldset>
  					<fieldset class="form-group">
  						<label for="prenom">Prenom :</label>
  						<input type="text" class="form-control" id="prenom" placeholder="Prénom" onblur="verifPrenom(this)" name="prenom">
  						<div id="prenom_alert" class="text-danger hidden">Veuillez entrer un prénom</div>
  					</fieldset>
  					<fieldset class="form-group">
  						<label for="datenaiss">Date de naissance :</label>
  						<input type="textarea" class="form-control" id="datenaiss" onblur="verifDDNEtAge(this)" name="datenaiss">
  						<div id="datenaiss_alert" class="text-danger hidden">Veuillez entrer une date au format jj/mm/aaaa</div>
  						<div id="datenaiss_alert_maj" class="text-danger hidden">Vous devez être majeur pour vous inscrire</div>
  					</fieldset>
  					<fieldset class="form-group">
  						<label for="email">Adresse e-mail :</label>
  						<input type="email" class="form-control" id="email" placeholder="Email" onblur="verifMail(this)" name="email">
  						<div id="email_alert" class="text-danger hidden">Veuillez entrer une adresse mail valide.</div>
  					</fieldset>
  					<fieldset class="form-group">
  						<label for="mdp">Mot de passe : </label>
  						<input type="password" class="form-control" id="mdp" placeholder="Mot de passe" onblur="verifMDP(this)" name="mdp">
  						<div id="mdp_alert" class="text-danger hidden">Veuillez entrer un mot de passe</div>
  					</fieldset>
  					<fieldset class="form-group">
  						<label for="confmdp">Confirmer le mot de passe : </label>
  						<input type="password" class="form-control" id="confmdp" placeholder="Confirmation" onblur="verifConfMDP(formInsc)">
  						<div id="conf_alert" class="text-danger hidden">Les mots de passe diffèrent</div>
  					</fieldset>
  					<button type="submit" class="btn btn-primary">S'inscrire</button>
  				</form>
  			</div>
  			<div class="col-md-3">
  			</div>
  		</div>
  		
  		<!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
        
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <p id="modal-text"></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
        
          </div>
        </div>
  		
  		<hr>

  		<footer>
  			<p>&copy;</p>
  		</footer>
  	</div> <!-- /container -->
  	
    <?php
      	if(isset($_POST['email']))
      	{
      		$base = mysql_connect ($bdd_adress, $bdd_login, $bdd_password) or die("Impossible de se connecter : " . mysql_error());
      		mysql_select_db ($bdd_name, $base);
      		mysql_query("SET character_set_results=utf8", $base);
      		mb_language('uni'); 
      		mb_internal_encoding('UTF-8');
      		mysql_query("set names 'utf8'",$base);
      		
      		$sql = 'SELECT u_email FROM utilisateur WHERE u_email = "'.$_POST['email'].'";';
      		$requete = mysql_query($sql) or die ("Erreur requete ".$sql." ==> ".mysql_error());
      		$verif = mysql_fetch_array($requete);
      		$date = date('Y-m-d');
      		if(empty($verif['u_email']))
      		{
      		    $mdp = sha1($CSecrete1.$_POST['mdp'].$CSecrete2);
      			$sql = 'INSERT INTO utilisateur(u_email, u_nom, u_prenom, u_mdp, u_date_naissance, u_role ,u_date_inscription) VALUES ("'.$_POST['email'].'","'.$_POST['nom'].'","'.$_POST['prenom'].'","'.$mdp.'","'.$_POST['datenaiss'].'","user","'.$date.'");';
      			mysql_query($sql) or die(mysql_error());
      			echo    "<script type='text/JavaScript'> 
      			            alert('Inscription reussie!');
      			            document.location.href='index.php';
      			        </script>";
      		}else{
      		    echo"
      		    <script type='text/JavaScript'> 
  		            alert('Il existe déjà un utilisateur avec cet e-mail!');
  		        </script>";
      		}
    
      		mysql_close($base);
      	}
  	?>  


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/JavaScript">
    
    function verifForm(f)
    {
    	var nom = verifNom(f.nom);
    	var prenom = verifPrenom(f.prenom);
    	var age = verifDDNEtAge(f.datenaiss);
    	var email = verifMail(f.email);
    	var mdp = verifMDP(f.mdp);
    	var confmdp = verifConfMDP(f);
    	
    	if(nom && prenom && age && email && mdp && confmdp)
    	{
    		return true;
    	}
    	else
    	{
    	    $("#modal-text").text("Veuillez remplir correctement tous les champs.");
            $('#myModal').modal('toggle');
    		return false;
    	}
    }
    
    function verifNom(champ)
    {
    	if (champ.value.length != 0)
    	{
    		surligne(champ,false);
    		$("#nom_alert").addClass("hidden");
    		return true;
    	}
    	else
    	{
    		surligne(champ,true);
    		$("#nom_alert").removeClass("hidden");            
    		return false;
    	}
    }
    
    function verifPrenom(champ)
    {
    	if (champ.value.length != 0)
    	{
    		surligne(champ,false);
    		$("#prenom_alert").addClass("hidden");
    		return true;
    	}
    	else
    	{
    		surligne(champ,true);
    		$("#prenom_alert").removeClass("hidden");
    		return false;
    	}
    }
    
    function verifDDNEtAge(champ)
    {	
    	//verifie que c'est une date sous le format  jj/dd/aaaa
    	//verifie que la personne a plus de 18 ans 
    	var decoupe = champ.value.split('/');
    	
    	var vdate = new Date(decoupe[2], decoupe[1]-1, decoupe[0]);
    	var now = new Date();
    	var ageMin = new Date(now.getFullYear()-18,now.getMonth(),now.getDate());
    	if (vdate.getFullYear() == decoupe[2] && vdate.getMonth()+1 == decoupe[1] && vdate.getDate() == decoupe[0] && ageMin > vdate)
    	{
    	    
    		surligne(champ,false);
    		$("#datenaiss_alert").addClass("hidden");
    		$("#datenaiss_alert_maj").addClass("hidden");
    		return true;
    	}
    	else
    	{
    	    if(ageMin < vdate){
    	        surligne(champ,true);
    	        $("#datenaiss_alert").addClass("hidden");
    		    $("#datenaiss_alert_maj").removeClass("hidden");
    		    return false;
    	    }
    		surligne(champ,true);
    		$("#datenaiss_alert_maj").addClass("hidden");
    		$("#datenaiss_alert").removeClass("hidden");
    		return false;
    	}
    }
    
   
    function verifMail(champ)
    {
    	var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
    	if(!regex.test(champ.value))
    	{
    		surligne(champ, true);
    		$("#email_alert").removeClass("hidden");
    		return false;
    	}
    	else
    	{
    		surligne(champ, false);
    		$("#email_alert").addClass("hidden");
    		return true;
    	}
    }
    
    function verifMDP(champ)
    {
    	if (champ.value.length != 0)
    	{
    		surligne(champ,false);
    		$("#mdp_alert").addClass("hidden");
    		return true;
    	}
    	else
    	{
    		surligne(champ,true);
    		$("#mdp_alert").removeClass("hidden");
    		return false;
    	}
    }
    
    function verifConfMDP(form)
    {
    	if(form.mdp.value != form.confmdp.value)
    	{
    		surligne(form.confmdp,true);
    		$("#conf_alert").removeClass("hidden");
    		return false;
    	}
    	else
    	{
    		surligne(form.confmdp,false);
    		$("#conf_alert").addClass("hidden");
    		return true;
    	}
    }
    
    function surligne(champ, erreur)
    {
    	if(erreur)
    		champ.style.backgroundColor = "#fba";
    	else
    		champ.style.backgroundColor = "";
    	
    }
    
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   
  	
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
