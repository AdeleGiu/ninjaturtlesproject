<?php
session_start();	
include_once('info_bdd.php');
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Historique de recherche</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	?>

  	<!-- Main jumbotron for a primary marketing message or call to action -->
  	<div class="jumbotron">
  		<div class="container">
  			<h1>Mon historique</h1>
  			<p></p>
  		</div>
  	</div>

  	<div class="container" id="histo_table">
  	  <table class="table table-striped">
			<thead>
     		 	<tr>
       				<th>Type</th>
        			<th>Date</th>
        			<th>Parametres de recherche </th>
        			<th>Lien vers la recherche </th>
      			</tr>
   			</thead>
			<tbody>
			

  		<!-- Example row of columns -->
  		
  		<?php
  		$base = mysql_connect ($bdd_adress, $bdd_login, $bdd_password) or die("Impossible de se connecter : " . mysql_error());
  		mysql_select_db ($bdd_name, $base);
  		mysql_query("SET character_set_results=utf8", $base);
  		mb_language('uni'); 
  		mb_internal_encoding('UTF-8');
  		mysql_query("set names 'utf8'",$base);
  		setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
  		  //	echo $_SESSION['role'];
	
  		if (isset($_SESSION['login']) and $_SESSION['role']=="user")
  		{	
  		    
            $sql = 'SELECT * FROM history WHERE (history.u_mail="'.$_SESSION['login'].'")ORDER BY history.h_date_research DESC';
  		    $commds = mysql_query($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
  		    
  		    $total = 0;
  		    $id = "bidon";
  	        
  	       
      		while($donnees = mysql_fetch_array($commds))
      		{  
      			$type = "";//$donnees['h_type'];
      			$album = "album=";
      			$track= "&track=";
      			$artist= "&artist=";
      			$label= "&label=";
      			$genre= "&genre=";
      			$ligne = "";
        	    if( $donnees['h_type']=="music"){ 
        	    	 
        	    	$ligne .=  "<tr>
       								 <td>".$donnees['h_type']."</td>
        							 <td>".strftime('%d %B %Y', strtotime($donnees['h_date_research']))."</td>
        							 <td>	 
        						";
        	    	
        	    	if($donnees['h_album']!= null){
        	    		$ligne .= $donnees['h_album'];
        	    		$album .= $donnees['h_album'];
        	    	}
        	    	if($donnees['h_artist']!= null){
        	    		$ligne .= " ".$donnees['h_artist'];
        	    		$artist .= $donnees['h_artist'];

        	    	}
        	    	if($donnees['h_track']!= null){
        	    		$ligne .=" ".$donnees['h_track'];
        	    		$track .= $donnees['h_track'];
        	    	}
        	    	if($donnees['h_label']!= null){
        	    		$ligne .=" ".$donnees['h_label'];
        	    		$label .= $donnees['h_label'];
        	    	}
        	    	if($donnees['h_genre']!= null){
        	    		$ligne .=" ".$donnees['h_genre'];
        	    		$genre .= $donnees['h_genre'];
        	    	}        	    	
          		     $ligne .= "</td>
          		     			<td><a href=\"music.php?".$type.$album.$artist.$track.$label.$genre."\" >Rechercher</a></td>
          		     		</tr>
          		     		" ;   
          		  echo $ligne;     			       
          		}else if($donnees['h_type']=="film"){
        	    	$ligne .=  "<tr>
       								 <td>".$donnees['h_type']."</td>
        							 <td>".strftime('%d %B %Y', strtotime($donnees['h_date_research']))."</td>
        							 <td>	 
        						";
        	    	
        	    	if($donnees['h_album']!= null){
        	    		$ligne .= $donnees['h_album'];
        	    		$album .= $donnees['h_album'];
        	    	}
        	    	if($donnees['h_artist']!= null){
        	    		$ligne .= " ".$donnees['h_artist'];
        	    		$artist .= $donnees['h_artist'];

        	    	}
        	    	if($donnees['h_genre']!= null){
        	    		$ligne .=" ".$donnees['h_genre'];
        	    		$genre .= $donnees['h_genre'];
        	    	}        	    	
          		     $ligne .= "</td>
          		     			<td><a href=\"film.php?".$type.$album.$artist.$genre."\" >Rechercher</a></td>
          		     		</tr>
          		     		" ;     
          		  echo $ligne;           		
          		 }else if($donnees['h_type']=="art"){
          		   $ligne .=  "<tr>
       								 <td>".$donnees['h_type']."</td>
        							 <td>".strftime('%d %B %Y', strtotime($donnees['h_date_research']))."</td>
        							 <td>	 
        						";
        	    	if($donnees['h_artist']!= null){
        	    		$ligne .= " ".$donnees['h_artist'];
        	    		$artist .= $donnees['h_artist'];

        	    	}
        	    	if($donnees['h_genre']!= null){
        	    		$ligne .=" ".$donnees['h_genre'];
        	    		$genre .= $donnees['h_genre'];
        	    	}        	    	
          		     $ligne .= "</td>
          		     			<td><a href=\"art.php?".$type.$artist.$genre."\" >Rechercher</a></td>
          		     		</tr>
          		     		" ;   
          		  echo $ligne;     
          	    }    		                 
  	        };
  	      
  		};
  		?>
  			</tbody>
  		</table>
  		
  		<hr>

  		<footer>
  			<p>&copy;</p>
  		</footer>
  	</div> <!-- /container -->	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
