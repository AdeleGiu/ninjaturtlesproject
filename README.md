# README #

Le dossier dist contient le CSS/JS commun à toutes les pages.
Les pages sont ensuite directement dans dossier.

Les fichiers qui ne représentent pas des pages du site :
- fonction.php contient les fonctions php utilisées sur plusieurs pages.
- info_bdd.php contient les informations de connection à la base de données.
- navbar.php est la barre de navigation présente en haut de toutes les pages.

Les fichiers qui représentent des pages du site :
- admin.php : page de gestion de l'administrateur qui lui permet de supprimer des utilisateurs.
- art.php : Page recherche d'artiste.
- compte.php : Affichage des informations du compte d'une personne connectée.
- connect_error.php : Page affichée en cas d'erreur dans le coupe login/mdp.
- film.php : Page recherche de film.
- history.php : Affichage de l'historique d'une personne connectée.
- index.php : Page d'acceuil / affichage des favoris si personne connectée.
- inscription.php : Page d'inscription.
- modif_mail.php : Page pour modifier son mail pour une personne connectée.
- music.php : Page recherche de musique. 
- oubli_mdp.php : Page en cas d'oubli de mot de passe.


Pour se connecter en tant qu'admin : 
login : admin / mot de passe : admin

Pour accèder au site en ligne : 
https://pacome.herbin.info/NinjaTurtle/

