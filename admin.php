<?php
session_start();	
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Interface administrateur</title>

	<!-- Bootstrap core CSS -->
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body>

  	<?php
  	include_once("navbar.php");
  	
  	?>
  	
	<div class="jumbotron">
  		<div class="container">
  			<h1>Gestion administrateur</h1>
  			<p> </p>
  		</div>
  	</div>

  	<div class="container" id="histo_table">
  	  <table class="table table-striped">
			<thead>
     		 	<tr>
       				<th>Nom</th>
        			<th>Prénom</th>
        			<th>Mail</th>
        			<th></th>
        			
      			</tr>
   			</thead>
			<tbody>
  		
  	<?php
  	$base = mysql_connect ($bdd_adress, $bdd_login, $bdd_password) or die("Impossible de se connecter : " . mysql_error());
  		mysql_select_db ($bdd_name, $base);
  		mysql_query("SET character_set_results=utf8", $base);
  		mb_language('uni'); 
  		mb_internal_encoding('UTF-8');
  		mysql_query("set names 'utf8'",$base);
  		setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
  	
  	$sql = 'SELECT * FROM utilisateur WHERE (utilisateur.u_role="user")';
  		    $commds = mysql_query($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
  		    
  		    $total = 0;
  		    $id = "bidon";
  	        
  	       
      		while($donnees = mysql_fetch_array($commds))
      		{  
      			$nom = "nom=";
      			$prenom="&prenom=";
      			$mail=$donnees['u_email'];
      			
      			$ligne = "";
        	    
        	    	 
        	    $ligne .=  "<tr><td>".$donnees['u_nom']."</td><td>".$donnees['u_prenom']."</td><td>".$donnees['u_email']."</td>";
       	
          		$ligne .=  "<td><button class='btn btn-primary' type='button' onclick='myDelete(\"".$mail."\")'>Supprimer</button></td></tr>";
          		 
          		
          		 echo $ligne;     			       
          		   		                 
  	        };
  	?>

		</tbody>
  			</table>
  		

  		<footer>
  			<p>&copy;</p>
  		</footer>
  	</div> <!-- /container -->	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
    
    function myDelete(mail){
    	
    	$.ajax({
    		type : "POST",
    		url : "fonction.php",
    		data : {email : mail,supp : mail},
    		dataType : 'json',
    		success : function(json){
    		
    		}, error: function(json){
    		
    		}
    	});
    	window.location.reload();
    };
    
    
    
	</script>
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	    <script src="dist/js/bootstrap.min.js"></script>
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	   
	</body>
	</html>
